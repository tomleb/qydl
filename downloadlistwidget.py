from PySide2 import QtCore
from PySide2.QtCore import QSortFilterProxyModel, Qt
from PySide2.QtGui import QKeySequence
from PySide2.QtWidgets import QAbstractItemView, QAction, QShortcut, QTreeView
from downloadlistdelegate import DownloadListDelegate
from downloadlistmodel import DownloadListModel
from preferences import Preferences

class DownloadListWidget(QTreeView):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.preferences = Preferences()
        self.setItemDelegate(DownloadListDelegate())
        self.download_model = DownloadListModel(self)
        self.proxy_model = QSortFilterProxyModel(self)
        self.proxy_model.setSourceModel(self.download_model)
        self.proxy_model.setDynamicSortFilter(True)
        self.setModel(self.proxy_model)

        self.setUniformRowHeights(True)
        self.header().setStretchLastSection(True)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.setSortingEnabled(True)
        self.setRootIsDecorated(False)
        self.setAllColumnsShowFocus(True)
        self.setItemsExpandable(False)
        self.setAutoScroll(True)

        self.header().sectionMoved.connect(self.save_settings)
        self.header().sectionResized.connect(self.save_settings)
        self.header().sortIndicatorChanged.connect(self.save_settings)

        self.create_actions()
        self.create_shortcuts()
        self.load_settings()

    def create_actions(self):
        self.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.remove_action = QAction(self.tr('Remove', 'Remove from list'))
        self.remove_action.triggered.connect(self.remove_selection)
        self.addAction(self.remove_action)

    def create_shortcuts(self):
        self.remove_shortcut = QShortcut(QKeySequence().Delete, self, None, None, Qt.WidgetShortcut)
        self.remove_shortcut.activated.connect(self.remove_selection)

    @QtCore.Slot()
    def remove_selection(self):
        selected = self.get_selected_downloads()
        for download_handle in selected:
            self.download_model.remove_download(download_handle)

    def get_selected_downloads(self):
        indices = self.selectionModel().selectedRows()
        def index_to_handle(index):
            source_index = self.map_to_source(index)
            return self.download_model.get_download_handle(source_index)
        return [index_to_handle(index) for index in indices]

    def map_to_source(self, index):
        if index.model() == self.proxy_model:
            return self.proxy_model.mapToSource(index)
        return index

    def load_settings(self):
        header_state = self.preferences.get_download_header_state()
        self.header().restoreState(header_state)

    def save_settings(self):
        header_state = self.header().saveState()
        self.preferences.set_download_header_state(header_state)
