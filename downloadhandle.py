from PySide2 import QtCore
from PySide2.QtCore import QObject

class DownloadHandle(QObject):
    """
    Cannonical representation of a download.
    This includes for example its progress, size, name, etc
    """
    data_changed = QtCore.Signal()
    def __init__(self, name, link, parent=None):
        super().__init__(parent)
        self.name = name
        self.link = link
        self.eta = None
        self.progress = 0.0
        self.size = 'Unknown'
        self.speed = '0 B/s'

    def set_name(self, name):
        self.name = name
        self.data_changed.emit()

    def set_link(self, link):
        self.link = link
        self.data_changed.emit()

    def set_progress(self, progress):
        self.progress = progress
        self.data_changed.emit()

    def set_speed(self, speed):
        if speed:
            self.speed = speed
        else:
            self.speed = '0 B/s'
        self.data_changed.emit()

    def set_size(self, size):
        if size:
            self.size = size
        self.data_changed.emit()

    def set_eta(self, eta):
        if eta:
            self.eta = eta
        else:
            self.eta = u'\u221E'
        self.data_changed.emit()
