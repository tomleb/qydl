<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>DownloadListModel</name>
    <message>
        <location filename="downloadlistmodel.py" line="99"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="downloadlistmodel.py" line="101"/>
        <source>Link</source>
        <translation>Lien</translation>
    </message>
    <message>
        <location filename="downloadlistmodel.py" line="103"/>
        <source>Completed</source>
        <translation>Complété</translation>
    </message>
    <message>
        <location filename="downloadlistmodel.py" line="105"/>
        <source>ETA</source>
        <translation>ETA</translation>
    </message>
    <message>
        <location filename="downloadlistmodel.py" line="107"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="downloadlistmodel.py" line="109"/>
        <source>Speed</source>
        <translation>Vitesse</translation>
    </message>
</context>
<context>
    <name>DownloadListWidget</name>
    <message>
        <location filename="downloadlistwidget.py" line="40"/>
        <source>Delete</source>
        <comment>Delete the download</comment>
        <translatorcomment>Supprimer le téléchargement</translatorcomment>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="gydlpy.py" line="40"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="gydlpy.py" line="85"/>
        <source>Link already in download queue.</source>
        <translation>Le lien est déjà dans la liste de téléchargement.</translation>
    </message>
</context>
</TS>
