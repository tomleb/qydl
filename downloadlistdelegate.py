from PySide2.QtWidgets import (QApplication,
                               QStyle,
                               QStyleOptionProgressBar,
                               QStyledItemDelegate)
from downloadlistmodel import Column

class DownloadListDelegate(QStyledItemDelegate):
    """ Display a progress bar in the DL_PROGRESS column """
    def __init__(self, parent=None):
        super().__init__(parent)

    def paint(self, painter, option, index):
        if index.column() == Column.DL_PROGRESS.value:
            options = QStyleOptionProgressBar()
            options.minimum = 0
            options.maximum = 100
            options.text = str(index.data()) + '%'
            options.textVisible = True
            options.rect = option.rect
            options.progress = int(index.data())

            style = QApplication.style()
            style.drawControl(QStyle.CE_ProgressBar, options, painter)
        else:
            QStyledItemDelegate.paint(self, painter, option, index)


