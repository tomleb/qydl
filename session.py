from PySide2 import QtCore
from PySide2.QtCore import QObject, QThread, qDebug
from downloadhandle import DownloadHandle
from youtubedlwrapper import YTDLWrapper


class DownloadThread(QThread):
    """
    progress_changed signal updates the progress information
    (eg: % done, speed, etc)
    """
    finished_with_result = QtCore.Signal(QThread, DownloadHandle)
    progress_changed = QtCore.Signal()
    def __init__(self, download_handle, parent=None):
        super().__init__(parent)
        self.download_handle = download_handle

    def run(self):
        ytdl = YTDLWrapper()
        with ytdl.download_audio(self.download_handle) as p:
            for line in p.stdout:
                if self.isInterruptionRequested():
                    qDebug('Thread exiting')
                    p.kill()
                    return

                (percent, size, speed, eta) = ytdl.extract_progress(line.decode())
                if not percent:
                    continue

                self.download_handle.set_eta(eta)
                self.download_handle.set_progress(percent)
                self.download_handle.set_size(size)
                self.download_handle.set_speed(speed)
                self.progress_changed.emit()

        self.finished_with_result.emit(self, self.download_handle)

class Session(QObject):
    """
    The Session handles the actual download of audio.

    It spawns and keeps track of threads that run youtube-dl in the background.
    It also keeps track of a list of downloads.

    There are a few signals to notify downloads state (eg: new download,
    updated download, etc)
    """

    download_added = QtCore.Signal(DownloadHandle)
    download_removed = QtCore.Signal(DownloadHandle)
    download_updated = QtCore.Signal()
    download_finished = QtCore.Signal(DownloadHandle)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.threads = []

    @QtCore.Slot(DownloadHandle)
    def add_download(self, download_handle):
        qDebug('Added download to session: ' + download_handle.name)

        dl_thread = DownloadThread(download_handle)
        dl_thread.progress_changed.connect(self.handle_download_updated)
        dl_thread.finished.connect(dl_thread.deleteLater)
        dl_thread.finished_with_result.connect(self.handle_download_finished)
        dl_thread.start()
        self.threads.append(dl_thread)

        self.download_added.emit(download_handle)

    @QtCore.Slot(DownloadHandle)
    def remove_download(self, download_handle):
        thread = None
        for t in self.threads:
            if t.download_handle == download_handle:
                thread = t
                break

        if thread:
            thread.requestInterruption()
            thread.wait()
            self.threads.remove(thread)

        self.download_removed.emit(download_handle)
        qDebug('Removed download ' + download_handle.name)

    @QtCore.Slot()
    def handle_download_updated(self):
        self.download_updated.emit()

    @QtCore.Slot(QThread, DownloadHandle)
    def handle_download_finished(self, thread, download_handle):
        self.threads.remove(thread)
        self.download_finished.emit(download_handle)
