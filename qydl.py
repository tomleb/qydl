#!/usr/bin/env python

from PySide2 import QtCore, QtWidgets, QtConcurrent
from PySide2.QtCore import QLocale, QThread, QTranslator, qDebug

from progressindicator import QProgressIndicator
from downloadlistwidget import DownloadListWidget
from downloadhandle import DownloadHandle
from youtubedlwrapper import YTDLWrapper
from session import Session
import os
import sys

class FetchInfoThread(QThread):
    is_error = QtCore.Signal(str)
    found_information = QtCore.Signal(DownloadHandle)
    def __init__(self, link, parent=None):
        super().__init__(parent)
        self.link = link

    def run(self):
        ytdl = YTDLWrapper()
        success, info = ytdl.download_information(self.link)

        if not success:
            self.is_error.emit(info)
            return

        self.dh = DownloadHandle(info['title'], self.link)
        self.found_information.emit(self.dh)

class MainWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.fetch_thread = None
        self.download_text = QtWidgets.QLineEdit()
        self.download_text.setPlaceholderText('https://www.youtube.com/watch?v=XXXXXXXXXXX')
        self.download_text.returnPressed.connect(self.add_download)

        self.download_button = QtWidgets.QPushButton(self.tr('Download'))
        self.download_button.clicked.connect(self.add_download)

        self.indicator = QProgressIndicator(self)
        self.indicator.setAnimationDelay(60)
        self.indicator.startAnimation()
        self.indicator.setVisible(False)

        download_layout = QtWidgets.QHBoxLayout()
        download_layout.addWidget(self.download_text)
        download_layout.addWidget(self.indicator)
        download_layout.addWidget(self.download_button)

        self.error_label = QtWidgets.QLabel(self)
        self.error_label.setVisible(False)
        self.error_label.setWordWrap(True)
        self.error_label.setStyleSheet('QLabel { color: red; }')
        font = self.error_label.font()
        font.setPointSize(8)
        self.error_label.setFont(font)

        self.download_list = DownloadListWidget(self)

        self.session = Session()
        self.download_list.download_model.download_added.connect(self.session.add_download)
        self.download_list.download_model.download_removed.connect(self.session.remove_download)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(download_layout)
        layout.addWidget(self.error_label)
        layout.addWidget(self.download_list)

        self.setWindowTitle('Youtube Downloader')
        self.resize(600, 350)
        self.setLayout(layout)

        # Need to set focus at the very end otherwise the focus gets overwritten
        # when adding widgets
        self.download_text.setFocus()

    @QtCore.Slot()
    def add_download(self):
        link = self.download_text.text()
        if not link:
            return

        if self.download_list.download_model.download_exists(link):
            self.handle_fetch_error(self.tr('Link already in download queue.'))
            return

        if self.fetch_thread:
            return

        self.indicator.startAnimation()
        self.indicator.setVisible(True)
        self.download_button.setEnabled(False)

        self.fetch_thread = FetchInfoThread(link)
        self.fetch_thread.found_information.connect(self.handle_fetch_success)
        self.fetch_thread.is_error.connect(self.handle_fetch_error)
        self.fetch_thread.start()

    def handle_fetch_success(self, download_handle):
        self.error_label.setVisible(False)
        if self.fetch_thread:
            self.fetch_thread.wait()
        self.fetch_thread = None
        self.download_text.clear()
        self.download_list.download_model.add_download(download_handle)
        qDebug('Finished fetching information for ' + download_handle.name)
        self.handle_fetch_finished()

    def handle_fetch_error(self, error):
        if self.fetch_thread:
            self.fetch_thread.wait()
        self.fetch_thread = None

        qDebug(error)
        self.error_label.setText(error.strip())
        self.error_label.setVisible(True)
        self.handle_fetch_finished()

    def handle_fetch_finished(self):
        self.indicator.stopAnimation()
        self.indicator.setVisible(False)
        self.download_button.setEnabled(True)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    # Because PySide2 does not have qSetMessagePattern yet
    if not os.environ.get('QT_MESSAGE_PATTERN'):
        os.environ['QT_MESSAGE_PATTERN'] = '[%{type}] %{appname} (%{file}:%{line}) - %{message}'

    translator = QTranslator()
    language_code = QLocale().name().split('_')[0]
    if translator.load(f'translations/{language_code}'):
        qDebug('Loading translations of locale ' + QLocale().name())
        app.installTranslator(translator)

    widget = MainWindow()
    widget.show()
    sys.exit(app.exec_())
