from PySide2 import QtCore
from PySide2.QtCore import QAbstractTableModel, QModelIndex, Qt
from downloadhandle import DownloadHandle
from enum import Enum

class Column(Enum):
    DL_NAME = 0
    DL_LINK = 1
    DL_PROGRESS = 2
    DL_ETA = 3
    DL_SIZE = 4
    DL_SPEED = 5

class DownloadListModel(QAbstractTableModel):
    download_added = QtCore.Signal(DownloadHandle)
    download_removed = QtCore.Signal(DownloadHandle)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.downloads = []

    def download_exists(self, link):
        return any([handle.link == link for handle in self.downloads])

    @QtCore.Slot(DownloadHandle)
    def add_download(self, download_handle):
        row = len(self.downloads)

        download_handle.data_changed.connect(self.handle_download_updated)

        self.beginInsertRows(QModelIndex(), row, row)
        self.downloads.append(download_handle)
        self.download_added.emit(download_handle)
        self.endInsertRows()

    @QtCore.Slot(DownloadHandle)
    def remove_download(self, download_handle):
        row = self.downloads.index(download_handle)

        download_handle.data_changed.disconnect(self.handle_download_updated)

        self.beginRemoveRows(QModelIndex(), row, row)
        self.downloads.remove(download_handle)
        self.download_removed.emit(download_handle)
        self.endRemoveRows()

    @QtCore.Slot()
    def handle_download_updated(self):
        self.dataChanged.emit(self.index(0, 0), self.index(self.rowCount() - 1, self.columnCount() - 1));

    def get_download_handle(self, index):
        if not index.isValid():
            return None
        return self.downloads[index.row()]

    def rowCount(self, index=QModelIndex()):
        """ Returns the number of downloads. """
        return len(self.downloads)

    def columnCount(self, index=QModelIndex()):
        """ Returns the number of columns the model holds. """
        return len(Column)

    def data(self, index, role=Qt.DisplayRole):
        """
        Return None when no data, otherwise return data
        """
        if not index.isValid():
            return None

        if not 0 <= index.row() < len(self.downloads):
            return None

        if role == Qt.DisplayRole:
            download = self.downloads[index.row()]
            if index.column() == Column.DL_NAME.value:
                return download.name
            elif index.column() == Column.DL_LINK.value:
                return download.link
            elif index.column() == Column.DL_PROGRESS.value:
                return download.progress
            elif index.column() == Column.DL_ETA.value:
                return download.eta
            elif index.column() == Column.DL_SIZE.value:
                return download.size
            elif index.column() == Column.DL_SPEED.value:
                return download.speed
        elif role == Qt.TextAlignmentRole:
            if index.column() in [Column.DL_ETA.value, Column.DL_SIZE.value, Column.DL_SPEED.value]:
                return Qt.AlignRight
            else:
                return None

        return None

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        """ Set the headers to be displayed """
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                if section == Column.DL_NAME.value:
                    return self.tr('Name')
                elif section == Column.DL_LINK.value:
                    return self.tr('Link')
                elif section == Column.DL_PROGRESS.value:
                    return self.tr('Completed')
                elif section == Column.DL_ETA.value:
                    return self.tr('ETA')
                elif section == Column.DL_SIZE.value:
                    return self.tr('Size')
                elif section == Column.DL_SPEED.value:
                    return self.tr('Speed')
        elif role == Qt.TextAlignmentRole:
            if section in [Column.DL_ETA.value, Column.DL_SIZE.value, Column.DL_SPEED.value]:
                return Qt.AlignRight
            else:
                return super().headerData(section, orientation, role)

        return None

    def setData(self, index, value, role=Qt.EditRole):
        """
        Adjust the data depending on the given index and role.
        Here, nothing can be changed!
        """
        return False

    def flags(self, index):
        if not index.isValid():
            return Qt.ItemIsEnabled
        return Qt.ItemFlags(QAbstractTableModel.flags(self, index))
