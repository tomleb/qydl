from PySide2.QtCore import QSettings

class Preferences():
    """ Simple wrapper around QSettings """
    def __init__(self):
        self.settings = QSettings('QYDL', 'qydl')

    def get_download_header_state(self):
        return self.settings.value('downloadlist/header_state')

    def set_download_header_state(self, state):
        self.settings.setValue('downloadlist/header_state', state)
