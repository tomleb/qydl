# About

Very simple youtube-dl frontend purely for downloading bestaudio[ext=m4a]
audios.

## Screenshot

![Screenshot](screenshots/screenshot.png?raw=true)

## Install

```bash
$ virtualenv .virtualenv
$ source .virtualenv/bin/activate
$ pip install -r requirements.txt
# Updating the translation files and compiling them into a release
$ pyside2-lupdate qydl.pro
$ lrelease qydl.pro
```

## Mentions

Progress indicator was taken from
[https://github.com/mojocorp/QProgressIndicator](https://github.com/mojocorp/QProgressIndicator)
(LGPL licensed) and modified for PySide2

Much of the code is inspired from
[qBittorrent](https://github.com/qbittorrent/qbittorrent)

## Dependencies

- youtube-dl
- virtualenv
- pip
- pyside2-tools
- qt5-tools

## License

See [LICENSE](LICENSE).
