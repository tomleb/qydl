from PySide2.QtCore import QStandardPaths, qWarning
from subprocess import Popen, PIPE

class YTDLWrapper:
    def __init__(self):
        path = QStandardPaths()

        self.executable = path.findExecutable('youtube-dl')
        self.ffmpeg_executable = path.findExecutable('ffmpeg')
        self.ffmpeg_options = ['--ffmpeg-location',
                               self.ffmpeg_executable,
                               '--extract-audio',
                               '--audio-format',
                               'mp3']

        self.default_destination = path.standardLocations(path.DownloadLocation)[0]

        self.default_options = (['--newline',
                                 '--no-warnings',
                                 '--youtube-skip-dash-manifest',
                                 '--no-playlist',
                                 '--no-mtime'])
        self.default_download_audio_option = ['-f', 'bestaudio[ext=m4a]']
        self.default_extract_info = ['--get-id', '--get-title']

    def download_audio(self, download_handle):
        """ Download audio using Popen with stdout=PIPE """
        link = download_handle.link
        output = ['-o', self.default_destination + '/%(title)s.%(ext)s' ]
        post_processing = self.ffmpeg_options[:]
        if not self.has_ffmpeg():
            qWarning('Cannot post process audio ' + download_handle.name + ' because ffmpeg was not found')
            post_processing = []

        command = ([ self.executable, link ] +
                  self.default_options +
                  self.default_download_audio_option +
                  self.ffmpeg_options +
                  output)
        return Popen(command, stdout=PIPE, bufsize=1)

    def download_information(self, link):
        """ Attempt to fetch information from the link """
        command = [ self.executable, link ] + self.default_options + self.default_extract_info
        p = Popen(command, stdout=PIPE, stderr=PIPE, bufsize=1)
        out, err = p.communicate()
        if p.returncode == 0:
            lines = out.decode().split('\n')
            title = lines[0]
            id = lines[1]
            info = {
                    'id': id,
                    'title': title
                    }
            return (True, info)
        else:
            return (False, err.decode())

    def extract_progress(self, progress_line):
        """
        Example of a progress line:
            '[download]  87.9% of 21.22MiB at  3.35MiB/s ETA 00:00'
        Example of progress line when finished:
            '[download] 100% of 4.04MiB'
        """
        tokens = [token for token in progress_line.strip().split(' ') if token != '']
        if tokens[0] != '[download]':
            return (None, None, None, None)

        if tokens[1][-1] != '%':
            return (None, None, None, None)

        percent_completed = float(tokens[1][:-1])

        file_size = None
        download_speed = None
        eta = None
        if len(tokens) > 7:
            file_size = tokens[3]
            download_speed = tokens[5]
            eta = tokens[7]
        # When download is finished (see comment string)
        elif len(tokens) == 4:
            file_size = tokens[3]

        result = (percent_completed, file_size, download_speed, eta)

        return result

    def has_ffmpeg(self):
        return self.ffmpeg_executable != ''
